from fabric.api import local


def runserver():
    local(
        '''
        . venv/bin/activate
        cd server
        python manage.py runserver
        '''
    )


def shell():
    local(
        '''
        . venv/bin/activate
        cd server
        python manage.py shell
        '''
    )

def startsmtp():
    local(
        '''
        sudo python3 -m smtpd -n -c DebuggingServer localhost:25  
        '''
    )

def full_migrate():
    local(
        '''
        . venv/bin/activate
        cd server
        python manage.py makemigrations
        python manage.py migrate
        '''
    )
