from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, UserChangeForm
from .models import Account
from django.core.validators import RegexValidator, MinValueValidator


class LoginForm(AuthenticationForm, forms.Form):
    class Meta:
        model = Account

    username = forms.CharField(
        label='Login', max_length=100,
        required=True,
        widget=forms.widgets.TextInput(
            attrs={'class': 'field_username form-control my-1'}
        )
    )

    password = forms.CharField(
        label='Password', max_length=20,
        required=True,
        widget=forms.widgets.PasswordInput(
            attrs={'class': 'field_password form-control my-1'}
        )
    )


class RegisterForm(UserCreationForm, forms.Form):
    class Meta:
        model = Account
        fields = (
            'username', 'password1', 'password2',
            'first_name', 'last_name', 'email',
            'age', 'avatar', 'phone',
        )

    username = forms.CharField(
        label='Имя пользователя', max_length=100,
        required=True,
        widget=forms.widgets.TextInput(
            attrs={'class': 'field_username form-control my-1'}
        )
    )

    first_name = forms.CharField(
        label='Имя', max_length=100,
        required=True,
        widget=forms.widgets.TextInput(
            attrs={'class': 'field_username form-control my-1'}
        )
    )

    last_name = forms.CharField(
        label='Фамилия', max_length=100,
        required=True,
        widget=forms.widgets.TextInput(
            attrs={'class': 'field_username form-control my-1'}
        )
    )

    password1 = forms.CharField(
        label='Пароль', max_length=20,
        required=True,
        widget=forms.widgets.PasswordInput(
            attrs={'class': 'field_password form-control my-1'}
        )
    )

    password2 = forms.CharField(
        label='Подтверждение пароля', max_length=20,
        required=True,
        widget=forms.widgets.PasswordInput(
            attrs={'class': 'field_password form-control my-1'}
        )
    )

    email = forms.CharField(
        label='E-mail', max_length=80,
        required=True,
        widget=forms.widgets.EmailInput(
            attrs={'class': 'field_email form-control my-1'}
        )
    )

    phone = forms.CharField(
        validators=[RegexValidator(
            regex=r'\+7\d{10}|8\d{10}',
            message='Введите корректный номер телефона.'
        )],
        label='Телефон', max_length=12,
        widget=forms.widgets.TextInput(
            attrs={'class': 'field_phone form-control my-1'}
        )
    )

    age = forms.IntegerField(
        validators=[MinValueValidator(
            18, message='Вы слишком молоды!'
        )],
        label='Возвраст',
        required=True,
        widget=forms.widgets.NumberInput(
            attrs={'class': 'field_age form-control my-1'}
        )
    )


class UserEditForm(UserChangeForm):
    class Meta:
        model = Account
        fields = ('username', 'password', 'first_name', 'last_name', 'email', 'age', 'avatar')

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control my-1'
            field.help_text = ''
            if field_name == 'password':
                field.widget = forms.PasswordInput()
                field.widget.attrs['class'] = 'form-control my-1'
            if field_name == 'username':
                field.widget.attrs['readonly'] = True

    def clean_age(self):
        data = self.cleaned_data['age']
        if data < 18:
            raise forms.ValidationError("Вы слишком молоды!")

        return data
