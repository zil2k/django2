from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib import auth
from accounts.forms import LoginForm, RegisterForm, UserEditForm
from accounts.models import Account
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.template.response import TemplateResponse
import hashlib, random


def login_user(request):
    title = 'Вход'
    success_url = 'catalog:catalog'
    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(data=request.POST)

        if form.is_valid():
            user = form.cleaned_data.get('username')
            passwd = form.cleaned_data.get('password')
            auth_user = auth.authenticate(
                username=user,
                password=passwd
            )

            if auth_user and auth_user.is_active:
                auth.login(request, auth_user)
                return redirect(success_url)

    content = {'title': title, 'login_form': form}
    return render(request, 'accounts/login.html', content)


def register_user(request):
    success_url = reverse_lazy('accounts:register')
    form = RegisterForm()

    if request.method == 'POST':
        form = RegisterForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            user.is_active = False
            salt = hashlib.sha1(str(random.random()).encode('utf8')).hexdigest()[:6]
            user.activation_key = hashlib.sha1(
                (user.email + salt).encode('utf8')).hexdigest()
            user.save()
            email = user.email
            activation_key = user.activation_key
            activation_url = reverse_lazy('accounts:verify', args=[email, activation_key])
            send_mail(
                'Signin User',
                activation_url,
                from_email='info@test.ru',
                recipient_list=[email],
            )
            return TemplateResponse(
                request,
                'mainapp/response.html', {
                    'response':
                        f'''Регистрация прошла успешно!
                        Пожалуйства, активируйсте учетную запись по ссылке с Вашего E-mail адреса.
                        '''
                }
            )
    return render(request, 'accounts/register.html', {'form': form})


@login_required(login_url=reverse_lazy('accounts:login'))
def edit_user(request):
    title = 'редактирование'

    if request.method == 'POST':
        edit_form = UserEditForm(request.POST, request.FILES, instance=request.user)
        if edit_form.is_valid():
            edit_form.save()
            success_url = reverse_lazy('accounts:login')
            return redirect(success_url)
    else:
        edit_form = UserEditForm(instance=request.user)

    content = {'title': title, 'edit_form': edit_form}

    return render(request, 'accounts/edit.html', content)


@login_required(login_url=reverse_lazy('accounts:login'))
def logout(request):
    auth.logout(request)
    success_url = reverse_lazy('accounts:login')
    return redirect(success_url)


def verify(request, email, activation_key):
    print(email, activation_key)
    success_url = reverse_lazy('accounts:login')
    try:
        user = Account.objects.get(email=email)
        if user.activation_key == activation_key and not user.is_activation_key_expired():
            user.is_active = True
            user.save()
            auth.login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return TemplateResponse(
                request,
                'mainapp/response.html', {
                    'response': 'Ваша учетная запись успешно активирована.'
                }
            )
    except Exception as e:
        print(f'error activation user : {e.args}')
        return TemplateResponse(
            request,
            'mainapp/response.html', {
                'response': 'Что-то пошло не так при активации учетной записи.'
            }
        )
