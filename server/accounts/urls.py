from django.urls import path
from accounts.views import login_user, edit_user, logout, register_user, verify

app_name = 'accounts'

urlpatterns = [
    path('login/', login_user, name='login'),
    path('register/', register_user, name='register'),
    path('verify/<str:email>/<str:activation_key>', verify, name='verify'),
    path('edit/', edit_user, name='edit'),
    path('logout/', logout, name='logout'),
]
