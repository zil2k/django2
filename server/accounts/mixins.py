from django.shortcuts import redirect


class AdminGroupRequired:
    redirect_url = ''

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser or request.user.groups.filter(name='manage'):
            return super(AdminGroupRequired, self).dispatch(request, *args, **kwargs)
        return redirect(self.redirect_url)
