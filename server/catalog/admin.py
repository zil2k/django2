import datetime

from django.template.loader import render_to_string
from django.contrib import admin

# Register your models here.

from catalog.models import CatalogCategory, ProductDetail


@admin.register(ProductDetail)
class ProductAdmin(admin.ModelAdmin):
    list_display = [
        'name', 'category', 'picture',
        'price', 'modified', 'created',
        'is_new'
    ]

    search_fields = [
        'name', 'short_desc', 'cost'
    ]

    list_filter = [
        'category', 'modified', 'created'
    ]

    fieldsets = (
        (
            'Main', {
                'fields': ('name', 'category')
            }
        ),
        (
            'Content', {
                'fields': ('image', 'short_desc', 'price')
            }
        ),
    )

    def picture(self, obj):
        if obj.image is not None:
            return render_to_string(
                'server/picture.html',
                {'image': obj.image.url, 'name': obj.image.name}
            )
        return render_to_string(
            'server/picture.html',
            {'image': None, 'name': 'image not found'}
    )

    def is_new(self, obj):
        today = datetime.datetime.now()
        kek = obj.created
        if kek is not None:
            return obj.created.date() >= today.date()
        else:
            return False


admin.site.register(CatalogCategory)
