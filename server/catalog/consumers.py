import asyncio

from channels.consumer import  AsyncConsumer


class CommentConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        print('kek', event)
        await self.send({
            'type': 'websocket.accept'
        })
        await asyncio.sleep(5)
        await self.send({
            'type': 'websocket.close'
        })
