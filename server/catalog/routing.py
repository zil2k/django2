from django.urls import path
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from channels.auth import AuthMiddlewareStack
from .consumers import CommentConsumer

from catalog.views import (
    CatalogView, ProductDeleteView, ProductUpdateView,
    ProductDetailView, ProductCreateView, CategoryView
    )


application = ProtocolTypeRouter({
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter([
                path('catalog/', CommentConsumer),
            ])
        )
    )
})