from .products import (
    ProductDetail, ProductCreateView, ProductDetailView,
    ProductUpdateView, ProductDeleteView
)

from .category import (
    CatalogView, CategoryView
)
