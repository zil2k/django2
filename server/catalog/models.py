from django.db import models


# Create your models here.

class CatalogCategory(models.Model):
    name = models.CharField(
        verbose_name='имя',
        max_length=64,
        unique=True
    )
    description = models.TextField(
        verbose_name='описание',
        blank=True
    )

    modified = models.DateTimeField(
        auto_now=True
    )
    created = models.DateTimeField(
        auto_now_add=True
    )

    def __str__(self):
        return self.name


class ProductDetail(models.Model):
    category = models.ForeignKey(
        CatalogCategory,
        on_delete=models.CASCADE
    )
    name = models.CharField(
        verbose_name='имя продукта',
        max_length=255,
        unique=True
    )
    image = models.ForeignKey(
        'images.Image',
        on_delete=models.PROTECT
    )
    short_desc = models.CharField(
        verbose_name='краткое описание',
        max_length=100,
        blank=True,
        null=True
    )
    spec = models.TextField(
        verbose_name='характеристики',
        blank=True
    )
    description = models.TextField(
        verbose_name='подробное описание',
        blank=True, null=True
    )
    price = models.DecimalField(
        verbose_name='цена',
        max_digits=8,
        decimal_places=2,
        default=0
    )
    quantity = models.PositiveIntegerField(
        verbose_name='склад',
        default=0
    )
    modified = models.DateTimeField(
        auto_now=True
    )
    created = models.DateTimeField(
        auto_now_add=True
    )

    def __str__(self):
        return ' '.join((self.name, self.category.name))
