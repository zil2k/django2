from rest_framework import serializers
from rest_framework.response import Response
from .models import Purchase


class PurchaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Purchase
        kwargs = {}
        fields = [
            'user', 'created', 'is_active',
            'cost', 'count', 'items'
        ]