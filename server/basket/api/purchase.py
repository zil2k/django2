from rest_framework.viewsets import ModelViewSet
from basket.serializers import PurchaseSerializer
from rest_framework.pagination import PageNumberPagination
from basket.models import Purchase


# class StandardResultsSetPagination(PageNumberPagination):
#     page_size = 6
#     page_size_query_param = 'page_size'
#     max_page_size = 6
#

# class ProductViewSet(ModelViewSet):
#     queryset = ProductDetail.objects.all()
#     serializer_class = ProductSerializer
#     pagination_class = StandardResultsSetPagination
#
#     def get_queryset(self):
#         result = ''
#         for key, value in self.request.GET.items():
#             if key == 'id__in':
#                 result = list(map(int, value.split(',')))
#         if result:
#             return ProductDetail.objects.filter(id__in=result)
#         return ProductDetail.objects.all()

class PurchaseViewSet(ModelViewSet):
    queryset = Purchase.objects.all()
    serializer_class = PurchaseSerializer

