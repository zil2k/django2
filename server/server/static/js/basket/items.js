const renderItem = ({name}, count) => (
        `

  <form class="form-inline product-item">
    <div class="form-group mb-2">
    <input type="text" readonly class="form-control-plaintext"value="${name}">
    </div>
    <div class="form-group mx-sm-3 mb-2">
    <input type="number" class="form-control" value="${count}">
    </div>
   </form>

  `
    )
;
