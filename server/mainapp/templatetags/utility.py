from django import template

register = template.Library()

@register.simple_tag()
def get_value_by_key(source, key):
    if isinstance(source, dict):
        if key == 'name':
            return str(source.get(key)).title()
        return source.get(key)
    return source
